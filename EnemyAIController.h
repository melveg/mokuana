// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "EnemyAIController.generated.h"

/**
 * 
 */
UCLASS()
class SECRET_API AEnemyAIController : public AAIController
{
    GENERATED_BODY()
    
    //UPROPERTY(transient)
    UBlackboardComponent* BlackBoardBot;
    
    //UPROPERTY(transient)
    UBehaviorTreeComponent* BehaviorBot;
    
    virtual void Possess(class APawn *InPawn);
    
    void SetEnemy(class APawn *InPawn);
    
    protected:
    
    uint8 EnemyKeyId;
    uint8 EnemyLocationID;
    
    public:
    AEnemyAIController();
    
    UFUNCTION(BlueprintCallable, Category = Behavior)
    void SearchForEnemy();

    
};
