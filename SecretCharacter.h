// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/Character.h"
#include "SecretCharacter.generated.h"

UCLASS(config=Game)
class ASecretCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
    
    
    //Custom Methods
    virtual void BeginPlay();
    //definition of active/deactive flashlight
    void ToggleFlashLight();
    void StartSprint();
    void StopSprint();
    
public:
	ASecretCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;
    
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera)
    class USpotLightComponent* SpotLight;
    
    //The rain follows the actor
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera)
    class UParticleSystemComponent* PlayerRain;
    
    /*Define material for spotlight*/
    //UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Materials)
    //UMaterialInterface* FlashLightMat;
    
    //Flashlight status
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = SpotLight)
    bool fstatus;
    
    /* Audio Component */
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
    class UAudioComponent* ARain;
    
    /* Effect component - Post Processing Component */
    //UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = PostProcessVolume)
    //class UPostProcessComponent* CamEffect;

protected:

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	// End of APawn interface

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
    /*Get SpotLight object*/
    FORCEINLINE class USpotLightComponent* GetSpotLight() const { return SpotLight; }
};

