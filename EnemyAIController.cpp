// Fill out your copyright notice in the Description page of Project Settings.

#include "Secret.h"
#include "EnemyAIController.h"
#include "EnemyBot.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "SecretCharacter.h"


AEnemyAIController::AEnemyAIController(){
    BlackBoardBot = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackBoard Component Bot"));
    BehaviorBot = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("Behavior Component Bot"));
}

void AEnemyAIController::Possess(class APawn* InPawn){
    Super::Possess(InPawn);
    AEnemyBot* Bot = Cast<AEnemyBot>(InPawn);
    if(Bot && Bot->BehaviorBot){
        UBlackboardData *BlackboardData = Bot->BehaviorBot->BlackboardAsset;
        BlackBoardBot->InitializeBlackboard(*BlackboardData);
        EnemyKeyId = BlackBoardBot->GetKeyID("Enemy");
        EnemyLocationID = BlackBoardBot->GetKeyID("Destination");
        UBehaviorTree* BehaviorTBot = Bot->BehaviorBot;
        BehaviorBot->StartTree(*BehaviorTBot);
    }
}

void AEnemyAIController::SetEnemy(class APawn* InPawn){
    BlackBoardBot->SetValueAsObject("Enemy", InPawn);
    BlackBoardBot->SetValueAsVector("Destination", InPawn->GetActorLocation());
}

void AEnemyAIController::SearchForEnemy(){
    APawn* MyBot = GetPawn();
    if(MyBot==NULL){
        return;
    }
    const FVector MyLoc = MyBot->GetActorLocation();
    float BestDistSQ = MAX_FLT;
    ASecretCharacter* MyPawn = NULL;
    for(FConstPawnIterator It = GetWorld()->GetPawnIterator();It;++It){
        ASecretCharacter* TestPawn = Cast<ASecretCharacter>(*It);
        if(TestPawn){
            const float DistSQ = FVector::Dist(TestPawn->GetActorLocation(), MyLoc);
            if(DistSQ<BestDistSQ){
                BestDistSQ = DistSQ;
                MyPawn = TestPawn;
            }
        }
    }
    if(MyPawn){
        SetEnemy(MyPawn);
    }
}