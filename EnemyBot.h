// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "EnemyBot.generated.h"

UCLASS()
class SECRET_API AEnemyBot : public ACharacter
{
	GENERATED_BODY()

    public:
    
    UPROPERTY(EditAnywhere, Category = Behavior)
    class UBehaviorTree* BehaviorBot;
	// Sets default values for this character's properties
	AEnemyBot();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	
	
};
