// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "SecretGameMode.generated.h"

UCLASS(minimalapi)
class ASecretGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ASecretGameMode();
};



